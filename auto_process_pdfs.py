#!/usr/bin/python3

from argparse import ArgumentParser
from os import chdir
from pathlib import Path
from shutil import copy
from subprocess import run

from cookiecutter.main import cookiecutter
from slugify import slugify

parser = ArgumentParser()
parser.add_argument("INDIR", help="Directory full of pdfs to process")
parser.add_argument(
    "--outdir",
    default="processed-scans",
    help="Directory to drop output files in, default is ./processed-scans",
)
parser.add_argument(
    "--finish", action="store_true", help="Finish processing rather than starting"
)

parser.add_argument(
    "--process", action="store_true", help="Call scantailor on each dir."
)
args = parser.parse_args()

if args.process and args.finish:
    raise NotImplementedError("Only one of --process and --finish can be given")

indir = Path(args.INDIR)
outdir = Path(args.outdir)


if outdir.is_file():
    raise FileExistsError("outdir is a file!")
try:
    outdir.mkdir()
except FileExistsError:
    pass

for pdf in sorted(indir.glob("*.pdf")):
    dirname = slugify(pdf.stem)
    if args.finish:
        dirname = Path(dirname)
        chdir(dirname)
        run(["./finish.py"])
        chdir(Path("../"))

    elif args.process:
        scantailor_file = Path(dirname) / f"{dirname}.ScanTailor"
        print(scantailor_file)
        assert scantailor_file.exists()
        cmd = ["scantailor", str(scantailor_file.resolve())]
        run(cmd)

    else:
        extra_context = {"directory_name": dirname, "finals_dir": str(outdir.resolve())}
        cookiecutter(
            "cookiecutter-process-scan", no_input=True, extra_context=extra_context
        )
        dirname = Path(dirname)
        copy(pdf, dirname / "source")

        chdir(dirname)
        run(["./prepare.py"])
        chdir(Path("../"))
