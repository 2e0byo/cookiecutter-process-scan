#!/usr/bin/python3
from pathlib import Path


def prepare():
    from os import chdir
    from subprocess import run
    from tempfile import TemporaryDirectory

    print("Preparing")

    chdir(Path("source/tmp"))
    for inf in Path("../").glob("*.pdf"):
        cmd = [
            "stapler",
            "burst",
            inf.resolve(),
        ]
        run(cmd)

    # can we use pdfimages?
    pdfimages = True

    cmd = ["pdfimages", "-list"]
    for pdf in Path("./").glob("*.pdf"):
        images = run(cmd + [pdf], encoding="utf8", capture_output=True).stdout
        if len(images.strip().split("\n")) > 3:
            pdfimages = False
            break
    if not pdfimages:
        print("Using Imagemagick")

        with TemporaryDirectory(dir=".") as tmpdir:
            cmd = (
                "find . -name '*.pdf' | parallel --tmpdir="
                + tmpdir
                + " --bar --max-args 1 convert -density 300 {} {}.tif \\;"
            )
            run(cmd, shell=True)

    else:
        print("Using pdfimages")
        cmd = (
            "find . -name '*.pdf' | parallel --bar --max-args 1 "
            "pdfimages -j -tiff {} {}-img \\;"
        )
        run(cmd, shell=True)
    chdir(Path("../../"))


def generate_scantailor_file():

    import jinja2

    template_dir = Path("./templates").resolve()

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))

    template = env.get_template("minimal_template.ScanTailor")

    outdir = Path("./final").resolve()
    input_filenames = []

    # find out which format we used
    src = Path("./source/tmp")
    imgs = sorted(src.glob("*.tif*"))
    if len(imgs) == 0:
        imgs = sorted(src.glob("*.jpg"))

    i = 3
    for img in imgs:
        d = {"name": str(img.name), "id": i}
        input_filenames.append(d)
        i += 2

    scantailor_file = template.render(outdir=outdir, input_filenames=input_filenames)

    with Path("{{cookiecutter.directory_name}}.ScanTailor").open("w") as f:
        f.write(scantailor_file)


if __name__ == "__main__":
    prepare()
    generate_scantailor_file()
