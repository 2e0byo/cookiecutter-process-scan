#!/usr/bin/python3

from functools import partial

flushprint = partial(print, flush=True)


def finish():
    from os import chdir
    from pathlib import Path
    from subprocess import run

    from wand.image import Image

    chdir(Path("./final"))
    flushprint("Trimming", end="")
    for f in Path(".").glob("*.tif"):
        flushprint(".", end="")
        with Image(filename=f.resolve()) as i:
            i.trim()
            i.save(filename=f.with_name(f.stem + "-trim.tif"))
    flushprint(" Done")

    cmd = (
        'find . -maxdepth 1 -name "*-trim.tif" |'
        "parallel --bar --maxargs 1 "
        "-i{} env OMP_THREAD_LIMIT=1 tesseract {} {}-ocr -l eng pdf \\;"
    )
    run(cmd, shell="True")

    cmd = "stapler cat *-ocr.pdf {{cookiecutter.directory_name}}.pdf"
    run(cmd, shell=True)
    chdir(Path("../"))
    final = Path("./final/{{cookiecutter.directory_name}}.pdf").resolve()
    finals_dir = Path("{{cookiecutter.finals_dir}}/" + final.name)
    finals_dir.symlink_to(final)


if __name__ == "__main__":
    finish()
